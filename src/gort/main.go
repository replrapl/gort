package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/caiges/whatsup"
)

func main() {
	//api := slack.New(os.Getenv("GORT_SLACK_API_TOKEN"))

	http.HandleFunc("/whatsup", WhatsUpHandler)
	err := http.ListenAndServe("104.236.123.115:6001", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

// Handlers
func WhatsUpHandler(w http.ResponseWriter, req *http.Request) {
	whatsupResponse := WhatsUp()

	message := struct {
		Text string `json:"text"`
	}{whatsupResponse}

	js, err := json.Marshal(message)
	if err != nil {
		log.Println("Could not marshall JSON")
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// Config is an internal representation of the config file
type Config struct {
	Urls  []map[string]string
	Slack map[string]string
}

func WhatsUp() string {
	configFile, err := os.Open("config/config.json")

	if err != nil {
		log.Fatal("Could not open config")
	}

	decoder := json.NewDecoder(configFile)
	config := Config{}
	err = decoder.Decode(&config)

	if err != nil {
		log.Fatal("Could not parse config")
	}

	contents := whatsup.GetContents(config.Urls)

	formattedContents := ""

	for _, content := range contents {
		formattedContents += fmt.Sprintf("%s -- %s-- %s\n", content.Project, content.Env, content.Version)
	}

	return formattedContents
}